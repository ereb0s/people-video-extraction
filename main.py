import cv2
import numpy as np
import tensorflow as tf
from PIL import Image, ImageOps
import os
import matplotlib.pyplot as plt
import argparse

def prepare_image(image, input_size):
    old_size = image.size  # old_size is in (width, height) format
    desired_ratio = input_size[0] / input_size[1]
    old_ratio = old_size[0] / old_size[1]

    if old_ratio < desired_ratio:
        new_size = (old_size[0], int(old_size[0] / desired_ratio))
    else:
        new_size = (int(old_size[1] * desired_ratio), old_size[1])

    delta_w = new_size[0] - old_size[0]
    delta_h = new_size[1] - old_size[1]
    padding = (delta_w // 2, delta_h // 2, delta_w - (delta_w // 2), delta_h - (delta_h // 2))
    cropped_image = ImageOps.expand(image, padding)

    # Resize the cropped image to the desired model size
    resized_image = cropped_image.convert('RGB').resize(input_size, Image.BILINEAR)

    # Convert to a NumPy array, add a batch dimension, and normalize the image.
    image_for_prediction = np.asarray(resized_image).astype(np.float32)
    image_for_prediction = np.expand_dims(image_for_prediction, 0)
    image_for_prediction = image_for_prediction / 127.5 - 1

    return image_for_prediction, cropped_image

def extract_people_bboxes(segmented_img):
    gray = cv2.cvtColor(segmented_img, cv2.COLOR_BGR2GRAY)
    contours, _ = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    extracted_people = []

    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        extracted_person = segmented_img[y:y+h, x:x+w]
        extracted_people.append(extracted_person)

    return extracted_people

def process_frame(frame, interpreter, input_size, input_details):
    print('processing frame')
    image = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
    image_for_prediction, cropped_image = prepare_image(image, input_size)

    interpreter.set_tensor(input_details[0]['index'], image_for_prediction)
    interpreter.invoke()
    print('invoked')
    
    raw_prediction = interpreter.tensor(interpreter.get_output_details()[0]['index'])()
    width, height = cropped_image.size
    seg_map = tf.argmax(tf.image.resize(raw_prediction, (height, width)), axis=3)
    print('seg_map: ', (seg_map.numpy()[0] == 13).shape)
    np_img_rgb = cv2.cvtColor(np.array(cropped_image), cv2.COLOR_RGB2BGR)
    seg_mask = (seg_map.numpy()[0] == 13).reshape(*np_img_rgb.shape[:2], 1).astype(np.int32)

    np_img_rgb_segged = np_img_rgb * seg_mask
    people = extract_people_bboxes(np_img_rgb_segged.astype(np.uint8))
    filtered_people = [p for p in people if min(p.shape[:2]) > 100]

    return filtered_people

def main(video_path, output_folder):
    # Load the model.
    tflite_path = 'lite-model_deeplabv3-xception65-ade20k_1_default_2.tflite'
    interpreter = tf.lite.Interpreter(model_path=tflite_path)
    input_details = interpreter.get_input_details()
    interpreter.allocate_tensors()

    # get image size
    input_size = input_details[0]['shape'][2], input_details[0]['shape'][1]

    cap = cv2.VideoCapture(video_path)
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    count = 0
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        if count % fps == 0:  # process one frame every second
            filtered_people = process_frame(frame, interpreter, input_size, input_details)
            for idx, person in enumerate(filtered_people):
                save_path = os.path.join(output_folder, f"frame_{count}_person_{idx}.png")
                cv2.imwrite(save_path, person)
        count += 1

    cap.release()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process video to extract persons from each frame.')
    parser.add_argument('video_path', type=str, help='Path to the video file.')
    parser.add_argument('output_folder', type=str, help='Folder to save the extracted persons.')
    args = parser.parse_args()

    if not os.path.exists(args.output_folder):
        os.makedirs(args.output_folder)
    
    main(args.video_path, args.output_folder)
